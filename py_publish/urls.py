"""py_publish URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings

from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from publish.views import signup, profile, ArticleFileUpdateView, edit_article, delete_article_file, \
    ArticleListView, create_article_file, delete_article, view_article, \
    download_document, upload_review, ArticleStageUpdateView, publish_article, reject_article, \
    accept_article, add_author, upload_article, temp_add, clear_temp, accept_without_modifications_article, \
    remove_t_author, remove_author

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', LoginView.as_view(template_name="account/login.html", redirect_authenticated_user=True),
         name='login'),
    path('', LoginView.as_view(template_name="account/login.html", redirect_authenticated_user=True), name='login'),
    path('logout/', LogoutView.as_view(template_name="account/logout.html"), name='logout'),
    path('signup/', signup, name='signup'),
    path('profile/', profile, name='profile'),
    path('article-upload/', upload_article, name='upload_article'),
    path('article-list/', login_required(ArticleListView.as_view(template_name='account/article/article-list.html')),
         name='article-list'),
    path('author-add/<int:article_id>', add_author, name='add_author'),
    path('author-remove/<int:author_id>', remove_author, name='remove_author'),
    path('article-update/<int:article_id>/', edit_article, name='update_article'),
    path('article-file-update/<int:pk>/', login_required(ArticleFileUpdateView.as_view(
        template_name='account/article_file/article_file_update.html')),
         name='update_article_file'),
    path('article-file-create/<int:article_id>/', create_article_file,
         name='create_article_file'),
    path('article-delete/<int:article_id>/', delete_article,
         name='delete_article'),
    path('article/<int:article_id>/', view_article,
         name='view_article'),
    path('article-review/download/<int:article_file_id>', download_document,
         name='download_document'),
    path('article-review/upload/<int:article_id>', upload_review,
         name='upload_review'),
    path('article-stage/<int:pk>/', login_required(ArticleStageUpdateView.as_view(
        template_name='account/article/stage_article.html')),
         name='stage_article'),
    path('article-author-add/', temp_add,
         name='author_add'),
    path('article-author-clear/', clear_temp,
         name='clear_temp'),
    path('article-author-remove/<int:t_author_id>', remove_t_author,
         name='remove_t_author'),
    path('article-file-delete/<int:article_file_id>', delete_article_file, name='delete_article_file'),
    path('article-publish/<int:article_id>', publish_article, name='publish_article'),
    path('article-reject/<int:article_id>', reject_article, name='reject_article'),
    path('article-accept/<int:article_id>', accept_article, name='accept_article'),
    path('article-accept-without-modifications/<int:article_id>', accept_without_modifications_article
         , name='accept_without_modifications_article'),

    path('password-reset/', auth_views.PasswordResetView.as_view(
        template_name='account/password_reset/password_reset.html'),
         name='password_reset'),

    path('password-reset/done/', auth_views.PasswordResetDoneView.as_view(
        template_name='account/password_reset/password_reset_done.html'),
         name='password_reset_done'),

    path('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(
             template_name='account/password_reset/password_reset_confirm.html'
         ),
         name='password_reset_confirm'),

    path('password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(
        template_name='account/password_reset/password_reset_complete.html'),
         name='password_reset_complete'),

]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
