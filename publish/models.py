import os

from PIL import Image
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
# Create your models here.
from django.db import models
from django.contrib.auth.models import PermissionsMixin

from py_publish.settings import MEDIA_ROOT

Q1_CHOICE = [
    ('YES', 'Yes, authors have taken account of all relevant comments.'),
    ('PARTIALLY', 'Authors have taken account of some relevant comments, but some comments are not considered.'),
    ('NO', 'Authors did not revise the paper properly.')
]
Q5_CHOICE = [
    ('ACCEPT', 'Definitely accept'),
    ('MINOR', 'Minor revisions required before accept'),
    ('MAJOR', 'Major revisions required before accept'),
    ('REJECT', 'Definitely reject')
]


class UserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, username, password=None):
        if not email:
            raise ValueError("Users must have an email")
        if not first_name:
            raise ValueError("Users must have a first_name")
        if not last_name:
            raise ValueError("Users must have a last_name")
        if not username:
            raise ValueError("Users must have a username")

        user = self.model(
            email=self.normalize_email(email),
            last_name=last_name,
            first_name=first_name,
            username=username
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, last_name, first_name, password):
        user = self.create_user(email, first_name, last_name, username, password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    ROLES = [
        ('AUTHOR', 'author'),
        ('EDITOR', 'editor'),
        ('REVIEWER', 'reviewer')
    ]

    email = models.EmailField(max_length=254, unique=True, verbose_name='email')
    username = models.CharField(max_length=254, unique=True, verbose_name='username')
    first_name = models.CharField(max_length=254)
    last_name = models.CharField(max_length=254)
    email_verified = models.BooleanField(default=False)
    role = models.CharField(max_length=254, null=True, default=None)
    user_role = models.CharField(max_length=20, choices=ROLES, default='AUTHOR')
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'username']

    def __str__(self):
        return self.email + ", " + self.last_name + ", " + self.first_name + ", " + self.username


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')

    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)


class Article(models.Model):
    ARTICLE_TYPES = [
        ('RESEARCH', 'Research'),
        ('REVIEW', 'Review'),
        ('LETTER', 'Letter')
    ]

    ARTICLE_STAGES = [
        ('DRAFT', 'Draft'),
        ('OPEN', 'In review'),
        ('ACCEPTED', 'Accepted for publication'),
        ('ACCEPTED_WITHOUT_MODIFICATIONS', 'Accepted without modifications for publication'),
        ('REJECTED', 'Rejected'),

    ]

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    type = models.CharField(max_length=20, choices=ARTICLE_TYPES, default='RESEARCH')
    stage = models.CharField(max_length=31, choices=ARTICLE_STAGES, default='DRAFT')
    uploaded_at = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=255)


class ArticleFile(models.Model):
    file = models.FileField(upload_to='docs/')
    article = models.ForeignKey(Article, on_delete=models.CASCADE)

    def delete(self, *args, **kwargs):
        os.remove(os.path.join(MEDIA_ROOT, self.file.name))
        super(ArticleFile, self).delete(*args, **kwargs)


class Review(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    reviewer = models.ForeignKey(User, on_delete=models.CASCADE)

    q1 = models.CharField(max_length=256, choices=Q1_CHOICE, default='NO')

    q2 = models.CharField(max_length=600)

    q3 = models.CharField(max_length=600)

    q4 = models.CharField(max_length=600)

    q5 = models.CharField(max_length=256, choices=Q5_CHOICE, default='REJECT')

    q6 = models.CharField(max_length=600)

    created_date = models.DateTimeField(auto_now_add=True)


class Author(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    university = models.CharField(max_length=255)
    department = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)


class TAuthor(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    university = models.CharField(max_length=255)
    department = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
