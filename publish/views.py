import hashlib
import os
from datetime import datetime

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.core.paginator import Paginator
from django.http import Http404
from django.shortcuts import render, redirect
# Create your views here.
import pypandoc
from django.utils.decorators import method_decorator
from django.views.generic import ListView, UpdateView

from django.core.files.base import File
from docx import Document

from publish.decorators import *
from publish.forms import UserCreateForm, ProfileUpdateForm, UserUpdateForm, ArticleUploadForm, ArticleFileUpdateForm, \
    ArticleUpdateForm, ArticleFileUploadForm, ReviewCreateForm, AuthorUploadForm, TAuthorForm, QUESTIONS
from publish.models import Article, ArticleFile, Review, Author, TAuthor
from py_publish import settings

from py_publish.settings import MEDIA_ROOT


def signup(request):
    if request.user.is_authenticated:
        return redirect('profile')

    if request.method == 'POST':
        u_form = UserCreateForm(request.POST)
        if u_form.is_valid():
            u_form.save()
            messages.success(request, f'User created successfully!')
            return redirect('login')
    else:
        u_form = UserCreateForm()
    return render(request, 'account/signup.html', {
        'u_form': u_form,
    })


def handle_uploaded_files(request, article_form):
    files = request.FILES.getlist('file_field')
    article = Article.objects.create(user=request.user, type=article_form.cleaned_data['type'],
                                     title=article_form.cleaned_data['title'])
    for file in files:

        file_name = file.name
        path = MEDIA_ROOT + '/docs/' + file_name
        art_file = ArticleFile.objects.create(file=file, article_id=article.id)
        art_file.save()

        # print(file_name)
        if file_name.endswith('.docx'):
            #####check formatting#####

            formatting = is_formatted(path)
            if not formatting[0]:
                messages.warning(request, file_name + ' is missing the Introduction section!')
            if not formatting[1]:
                messages.warning(request, file_name + ' is missing the Motivation section!')
            if not formatting[2]:
                messages.warning(request, file_name + ' is missing the References section!')

            #####check formatting#####
            hs = hashlib.sha256(file_name.encode('utf-8')).hexdigest()
            _hs = hs[:7]
            # print(MEDIA_ROOT + '/docs/' + file_name)
            # convert the file to pdf
            output = pypandoc.convert_file(path, 'pdf',
                                           outputfile=path + '_' + _hs + '.pdf')
            art_file.delete()

            output_file = File(output)  # get the file
            output_file.file = 'docs/' + file_name + '_' + _hs + '.pdf'
            article_file = ArticleFile.objects.create(file=output_file.file, article_id=article.id)  # save it
            article_file.save()

    return article.id


def notify_on_submit(article_id, emails, user):
    article = Article.objects.get(pk=article_id)
    authors = Author.objects.filter(article_id=article_id)
    list_authors_name = ''
    for author in authors:
        if author == Author.objects.last():
            list_authors_name += author.last_name + ' ' + author.first_name
        else:
            list_authors_name += author.last_name + ' ' + author.first_name + ', '

    subject = article.title + ' has been submitted to py_publish!'
    message = 'Your article has been submitted.' + '\n' + \
              'Manuscript ID: ' + str(article_id) + '\n' + \
              'Type of manuscript: ' + article.get_type_display() + '\n' + \
              'Title: ' + article.title + '\n' + \
              'Authors: ' + list_authors_name + '\n' + \
              'Received: ' + str(datetime.now()) + '\n' + \
              'E - mails: ' + str(emails) + '\n' + \
              '*** This is an automatically generated email ***'

    for email in emails:
        send_mail(
            subject,
            message,
            '',
            [email],
            fail_silently=False,
        )


@login_required
@allowed_users(allowed_roles=['AUTHOR'])
def upload_article(request):
    if request.method == 'POST':

        t_author_list = TAuthor.objects.filter(user_id=request.user.id)

        article_form = ArticleUploadForm(request.POST, request.FILES)

        if article_form.is_valid():
            article_id = handle_uploaded_files(request, article_form)
            for t_author in t_author_list:
                Author.objects.create(first_name=t_author.first_name, last_name=t_author.last_name,
                                      email=t_author.email, university=t_author.university,
                                      department=t_author.department, article_id=article_id)

            t_author_emails = get_author_emails(article_id)
            notify_on_submit(article_id, t_author_emails, request.user)
            messages.success(request, f'Article saved as draft!')
            return redirect(request.META['HTTP_REFERER'])
    else:  # GET
        # we don't want to display the already saved model instances
        article_form = ArticleUploadForm()
        t_author_list = TAuthor.objects.filter(user_id=request.user.id)

    return render(request, 'account/article/article_upload_form.html', {
        'article_form': article_form,
        't_author_list': t_author_list
    })


def is_formatted(filename):
    doc = Document(filename)
    has_introduction = False
    has_motivation = False
    has_references = False
    for para in doc.paragraphs:
        if 'Introducere' in para.text or 'Introduction' in para.text:
            has_introduction = True
        if 'Motivatie' in para.text or 'Motivation' in para.text:
            has_motivation = True
        if 'Referinte' in para.text or 'References' in para.text:
            has_references = True
    return [has_introduction, has_motivation, has_references]


class ArticleListView(ListView):
    model = Article
    paginate_by = 5

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super(ArticleListView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super().get_queryset()

        if self.user.user_role == 'REVIEWER':
            return qs.filter(stage='OPEN').order_by('id')

        if self.user.user_role == 'EDITOR':

            reviews = Review.objects.all().order_by('id')
            article_list = []

            for article in Article.objects.all().order_by('id'):
                count = 0
                for review in reviews:
                    if review.article_id == article.id:
                        count = count + 1
                    if count == 2:
                        article_list.append(article)
                        break
            return article_list

        return qs.filter(user=self.user).order_by('id')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class ArticleFileUpdateView(UpdateView):
    model = ArticleFile
    form = ArticleFileUpdateForm
    fields = ['file']
    success_url = '?success'

    @method_decorator(only_author)
    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        self.pk = self.kwargs['pk']
        self.request = request
        article_file = ArticleFile.objects.get(pk=self.pk)
        article = Article.objects.get(pk=article_file.article_id)

        if article_file.article_id != article.id:
            raise PermissionDenied

        if request.user.id != article.user_id:
            raise PermissionDenied

        return super(ArticleFileUpdateView, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        return ArticleFile.objects.get(id=self.pk)

    def form_valid(self, form):
        file = form.cleaned_data['file']

        if file.name.endswith('.docx'):
            formatting = is_formatted(file)
            if not formatting[0]:
                messages.warning(self.request, f'File is missing the Introduction section!')
            if not formatting[1]:
                messages.warning(self.request, f'File is missing the Motivation section!')
            if not formatting[2]:
                messages.warning(self.request, f'File is missing the References section!')

        self.get_object(self).delete()

        return super().form_valid(form)


@login_required
@allowed_users(allowed_roles=['AUTHOR'])
def edit_article(request, article_id):
    article_files = ArticleFile.objects.filter(article_id=article_id).order_by('id')
    article = Article.objects.get(id=article_id)
    authors = Author.objects.filter(article_id=article_id)
    if request.user.id != article.user_id:
        raise PermissionDenied
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ArticleUpdateForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            article.type = form.cleaned_data['type']
            article.save()
            messages.success(request, f'Your article has been updated!')
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return render(request, 'account/article/article_update.html', {'article_form': form,
                                                                           'article_files': article_files,
                                                                           'article': article,
                                                                           'authors': authors})

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ArticleUpdateForm(instance=article)

    return render(request, 'account/article/article_update.html', {'article_form': form,
                                                                   'article_files': article_files,
                                                                   'article': article,
                                                                   'authors': authors})


@login_required
@allowed_users(allowed_roles=['AUTHOR'])
def create_article_file(request, article_id):
    article = Article.objects.get(pk=article_id)

    if request.user.id != article.user_id:
        raise PermissionDenied

    if request.method == 'POST':
        form = ArticleFileUploadForm(request.POST, request.FILES)
        if form.is_valid():
            article_file = ArticleFile.objects.create(file=request.FILES['file'], article_id=article_id)
            file = form.cleaned_data['file']

            if file.name.endswith('.docx'):
                formatting = is_formatted(file)
                if not formatting[0]:
                    messages.warning(request, file.name + f' is missing the Introduction section!')
                if not formatting[1]:
                    messages.warning(request, file.name + f' is missing the Motivation section!')
                if not formatting[2]:
                    messages.warning(request, file.name + f' is missing the References section!')

            article_file.save()
            return redirect('update_article', article_id=article_id)
    else:
        form = ArticleFileUploadForm()
    return render(request, 'account/article_file/article_file_create.html', {'form': form})


@login_required
@allowed_users(allowed_roles=['AUTHOR'])
def delete_article_file(request, article_file_id):
    article_file = ArticleFile.objects.get(id=article_file_id)
    article = Article.objects.get(id=article_file.article_id)

    count = 0

    for _ in ArticleFile.objects.filter(article_id=article.id):
        count = count + 1

    if count == 1:
        messages.warning(request, f'Article must have at least one file!')
        return redirect(request.META['HTTP_REFERER'])

    if request.user.id != article.user_id:
        raise PermissionDenied

    article_file.delete()
    return redirect(request.META['HTTP_REFERER'])


def notify_on_accept(user, emails, article_id):
    article = Article.objects.get(pk=article_id)
    authors = Author.objects.filter(article_id=article_id)
    reviews = Review.objects.filter(article_id=article_id)
    list_authors_name = ''
    for author in authors:
        if author == Author.objects.last():
            list_authors_name += author.last_name + ' ' + author.first_name
        else:
            list_authors_name += author.last_name + ' ' + author.first_name + ', '

    subject = article.title + ' has been accepted (with modifications)'
    message = 'Your article has been accepted (with modifications)' + '\n' + \
              'Manuscript ID: ' + str(article_id) + '\n' + \
              'Type of manuscript: ' + article.get_type_display() + '\n' + \
              'Title: ' + article.title + '\n' + \
              'Authors: ' + list_authors_name + '\n' + \
              'Received: ' + str(datetime.now()) + '\n' + \
              'E - mails: ' + str(emails) + '\n' + \
              '\n\n' + get_all_reviews_str(reviews) + \
              '*** This is an automatically generated email ***'

    for email in emails:
        send_mail(
            subject,
            message,
            'pytpublish@gmail.com',
            [email],
            fail_silently=False,
        )


def notify_on_publish(emails, article_id):
    article = Article.objects.get(pk=article_id)
    authors = Author.objects.filter(article_id=article_id)
    list_authors_name = ''
    for author in authors:
        if author == Author.objects.last():
            list_authors_name += author.last_name + ' ' + author.first_name
        else:
            list_authors_name += author.last_name + ' ' + author.first_name + ', '

    subject = article.title + ' has been published'
    message = 'Thank you very much for uploading the following manuscript to the py_publish system.' + '\n' + \
              'Manuscript ID: ' + str(article_id) + '\n' + \
              'Type of manuscript: ' + article.get_type_display() + '\n' + \
              'Title: ' + article.title + '\n' + \
              'Authors: ' + list_authors_name + '\n' + \
              'Received: ' + str(datetime.now()) + '\n' + \
              'E - mails: ' + str(emails) + '\n' + \
              '*** This is an automatically generated email ***'

    for email in emails:
        send_mail(
            subject,
            message,
            'pytpublish@gmail.com',
            [email],
            fail_silently=False,
        )


def notify_on_accept_without_modifications(user, emails, article_id):
    article = Article.objects.get(pk=article_id)
    authors = Author.objects.filter(article_id=article_id)
    list_authors_name = ''
    for author in authors:
        if author == Author.objects.last():
            list_authors_name += author.last_name + ' ' + author.first_name
        else:
            list_authors_name += author.last_name + ' ' + author.first_name + ', '

    subject = article.title + ' has been accepted (without modifications)'
    message = 'Your article has been accepted (without modifications)' + '\n' + \
              'Manuscript ID: ' + str(article_id) + '\n' + \
              'Type of manuscript: ' + article.get_type_display() + '\n' + \
              'Title: ' + article.title + '\n' + \
              'Authors: ' + list_authors_name + '\n' + \
              'Received: ' + str(datetime.now()) + '\n' + \
              'E - mails: ' + str(emails) + '\n' + \
              '*** This is an automatically generated email ***'

    for email in emails:
        send_mail(
            subject,
            message,
            'pytpublish@gmail.com',
            [email],
            fail_silently=False,
        )


def notify_on_reject(emails, article_id):
    article = Article.objects.get(pk=article_id)
    reviews = Review.objects.filter(article_id=article_id)
    subject = article.title + ' has been rejected!'
    message = 'Your article has been rejected.' + '\n\n' + get_all_reviews_str(reviews) + '\n' + \
              '*** This is an automatically generated email ***'
    for email in emails:
        send_mail(
            subject,
            message,
            '',
            [email],
            fail_silently=False,
        )


@login_required
@allowed_users(allowed_roles=['AUTHOR'])
def delete_article(request, article_id):
    article = Article.objects.get(id=article_id)
    if request.user.id != article.user_id:
        raise PermissionDenied

    if article.stage != 'DRAFT':
        if article.stage != 'REJECTED':
            raise PermissionDenied

    article_files = ArticleFile.objects.filter(article_id=article_id)
    for article_file in article_files:
        article_file.delete()
    article.delete()

    return redirect(request.META['HTTP_REFERER'])


@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST,
                                   request.FILES,
                                   instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your account has been updated!')
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'user': request.user,
        'u_form': u_form,
        'p_form': p_form
    }
    return render(request, 'account/profile.html', context)


@login_required
def view_article(request, article_id):
    article = Article.objects.get(pk=article_id)
    article_files = ArticleFile.objects.filter(article_id=article_id)

    if request.user.id != article.user_id:
        if request.user.user_role == 'AUTHOR':
            raise PermissionDenied
        elif is_editor(request.user) and get_count_reviews(Review.objects.filter(article_id=article_id)) < 2:
            return HttpResponse(f'Not enough reviews to view this article.')

    review_list = Review.objects.filter(article_id=article_id).order_by('id')
    paginator = Paginator(review_list, 10)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    current_user = request.user
    return render(request, 'account/article/view_article.html', {'article': article,
                                                                 'page_obj': page_obj,
                                                                 'user': current_user,
                                                                 'article_files': article_files,
                                                                 'questions': QUESTIONS
                                                                 })


@login_required
@allowed_users(allowed_roles=['REVIEWER'])
def upload_review(request, article_id):
    article = Article.objects.get(pk=article_id)

    if request.method == 'POST':
        r_form = ReviewCreateForm(request.POST, request.FILES)
        if r_form.is_valid():
            review = r_form.save(commit=False)
            review.article_id = article.id
            review.reviewer_id = request.user.id

            review.q1 = r_form.cleaned_data['q1']
            review.q2 = r_form.cleaned_data['q2']
            review.q3 = r_form.cleaned_data['q3']
            review.q4 = r_form.cleaned_data['q4']
            review.q5 = r_form.cleaned_data['q5']
            review.q6 = r_form.cleaned_data['q6']

            review.save()

            messages.success(request, f'Your review has been uploaded!')
            return redirect('view_article', article_id=article_id)
    else:
        r_form = ReviewCreateForm()
    return render(request, 'account/review/upload_review.html', {
        'r_form': r_form,

    })


@login_required
def download_document(request, article_file_id):
    article_file = ArticleFile.objects.get(pk=article_file_id)
    article = Article.objects.get(pk=article_file.article_id)

    if request.user.id != article.user_id:
        if request.user.role == 'AUTHOR':
            raise PermissionDenied

    file_path = os.path.join(settings.MEDIA_ROOT, article_file.file.path)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http404


def get_count_reviews(reviews):
    return reviews.count()


class ArticleStageUpdateView(UpdateView):
    model = Article
    success_url = '?success'
    fields = ['stage', ]

    def dispatch(self, request, *args, **kwargs):
        if request.user.user_role != 'EDITOR':
            raise PermissionDenied
        self.user = request.user
        self.pk = self.kwargs['pk']
        self.article = Article.objects.get(pk=self.pk)

        if get_count_reviews(Review.objects.all()) < 2:
            return HttpResponse("Not enough reviews to stage article.")

        return super(ArticleStageUpdateView, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        return Article.objects.get(pk=self.pk)

    def form_valid(self, form):
        article = Article.objects.get(pk=self.pk)
        if article.stage == 'REJECTED':
            pass
        return super().form_valid(form)


@login_required
@allowed_users(allowed_roles=['AUTHOR'])
def publish_article(request, article_id):
    article = Article.objects.get(pk=article_id)

    if request.user.id != article.user_id:
        raise PermissionDenied

    author_email_list = get_author_emails(article_id)

    article.stage = 'OPEN'
    article.save()
    notify_on_publish(author_email_list, article_id)
    messages.success(request, f'Article published!')
    return redirect(request.META['HTTP_REFERER'])


@login_required
@allowed_users(allowed_roles=['EDITOR'])
def accept_article(request, article_id):
    article = Article.objects.get(pk=article_id)

    if article.stage != 'OPEN':
        return HttpResponse('Cannot accept this article since it is not open for review')

    article.stage = 'ACCEPTED'
    article.save()
    author_email_list = get_author_emails(article_id)
    notify_on_accept(article.user, author_email_list, article_id)
    messages.success(request, f'Article accepted!')
    return redirect(request.META['HTTP_REFERER'])


@login_required
@allowed_users(allowed_roles=['EDITOR'])
def accept_without_modifications_article(request, article_id):
    article = Article.objects.get(pk=article_id)

    if article.stage != 'OPEN':
        return HttpResponse('Cannot accept this article since it is not open for review')

    article.stage = 'ACCEPTED_WITHOUT_MODIFICATIONS'
    article.save()

    author_emails = get_author_emails(article_id)
    notify_on_accept_without_modifications(article.user, author_emails, article_id)

    messages.success(request, f'Article accepted without modifications!')
    return redirect(request.META['HTTP_REFERER'])


@login_required
@allowed_users(allowed_roles=['EDITOR'])
def reject_article(request, article_id):
    article = Article.objects.get(pk=article_id)

    if article.stage != 'OPEN':
        raise PermissionDenied

    article.stage = 'REJECTED'

    author_emails = get_author_emails(article_id)

    notify_on_reject(author_emails, article_id)
    article.save()
    messages.success(request, f'Article rejected!')
    return redirect(request.META['HTTP_REFERER'])


@login_required
@allowed_users(allowed_roles=['AUTHOR'])
def add_author(request, article_id):
    article = Article.objects.get(pk=article_id)

    if request.user.id != article.user_id:
        raise PermissionDenied

    if request.method == 'POST':
        form = AuthorUploadForm(request.POST)
        if form.is_valid():
            author = Author.objects.create(
                email=form.cleaned_data['email'],
                first_name=form.cleaned_data['first_name'],
                last_name=form.cleaned_data['last_name'],
                university=form.cleaned_data['university'],
                department=form.cleaned_data['department'],
                article_id=article_id)
            author.save()
            return redirect('update_article', article_id=article_id)
    else:
        form = AuthorUploadForm()
    return render(request, 'account/article/add_author.html', {'form': form})


@login_required
@allowed_users(allowed_roles=['AUTHOR'])
def temp_add(request):
    if request.method == 'POST':

        author_form = TAuthorForm(request.POST)

        if author_form.is_valid():
            # extract data
            last_name = author_form.cleaned_data['last_name']
            first_name = author_form.cleaned_data['first_name']
            university = author_form.cleaned_data['university']
            department = author_form.cleaned_data['department']
            email = author_form.cleaned_data['email']

            TAuthor(last_name=last_name, first_name=first_name, university=university,
                    department=department, email=email, user_id=request.user.id).save()

            return redirect('upload_article')
    else:  # GET

        author_form = TAuthorForm()

    return render(request, 'account/article/temp_author_add.html', {
        'author_form': author_form
    })


@login_required
@allowed_users(allowed_roles=['AUTHOR'])
def clear_temp(request):
    TAuthor.objects.filter(user_id=request.user.id).delete()
    return redirect(request.META['HTTP_REFERER'])


def get_review_str(review):
    return (review.reviewer.username + '\n' + QUESTIONS[0] + '\n' + review.get_q1_display() + '\n'  # q1
            + QUESTIONS[1] + '\n' + review.q2 + '\n'  # q2
            + QUESTIONS[2] + '\n' + review.q3 + '\n'  # q3
            + QUESTIONS[3] + '\n' + review.q4 + '\n'  # q4
            + QUESTIONS[4] + '\n' + review.get_q5_display() + '\n\n'  # q5
            )


def get_all_reviews_str(reviews):
    s = ''
    for review in reviews:
        s += get_review_str(review)

    return s


def get_author_emails(article_id):
    authors = Author.objects.filter(article_id=article_id)
    author_emails = []
    for _ in authors:
        author_emails.append(_.email)
    return author_emails


@login_required
@allowed_users(allowed_roles=['AUTHOR'])
def remove_t_author(request, t_author_id):
    t_author = TAuthor.objects.get(pk=t_author_id)
    if request.user.id != t_author.user_id:
        raise PermissionDenied
    t_author.delete()
    return redirect(request.META['HTTP_REFERER'])


def is_author(user):
    return user.user_role == 'AUTHOR'


def is_reviewer(user):
    return user.user_role == 'REVIEWER'


def is_editor(user):
    return user.user_role == 'EDITOR'


@login_required
@allowed_users(allowed_roles=['AUTHOR'])
def remove_author(request, author_id):
    author = Author.objects.get(pk=author_id)
    article = Article.objects.get(pk=author.article_id)

    if request.user.id != article.user_id:
        raise PermissionDenied

    author.delete()
    messages.success(request, f'Author removed!')
    return redirect(request.META['HTTP_REFERER'])
