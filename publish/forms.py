from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm
from .models import User, Profile, Article, ArticleFile, Review, Author, TAuthor, Q1_CHOICE, Q5_CHOICE

QUESTIONS = [
    # q1
    'Have authors revised the paper by taking account of all relevant comments from ALL reviewers?',
    # q2
    'Please explain which comments of reviewers the authors missed out, and how they should have considered those comments?',
    # q3
    'Please comment on the quality of the revised paper? Is the paper worth publishing at this stage? Have any new problems come up in the revised paper?',
    # q4
    'Any other feedback for authors (Please do not duplicate your previous comments. Please consider providing any further comments that could help authors in improving this paper and/or their future research.)',
    # q5
    'My overall recommendation is',
    # q6 only editor
    'If this paper is marginal or unusual in some way, can you comment on anything else that might help the editors reach a decision?'
]


class UserCreateForm(UserCreationForm):
    first_name = forms.CharField(max_length=254, required=True)
    last_name = forms.CharField(max_length=254, required=True)
    email = forms.EmailField(max_length=254, required=True)
    username = forms.CharField(max_length=254, required=True)

    class Meta:
        model = User
        fields = ('email', 'user_role', 'first_name', 'last_name', 'username', 'password1', 'password2')


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ('email', 'last_name', 'first_name', 'username')


class AuthorUploadForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ('email', 'first_name', 'last_name', 'university', 'department')


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('image',)


class ArticleUploadForm(ModelForm):
    file_field = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))

    class Meta:
        model = Article
        fields = ('title', 'type')


class ArticleUpdateForm(ModelForm):
    class Meta:
        model = Article
        fields = ('type',)


class ArticleFileUpdateForm(ModelForm):
    file_field = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))

    class Meta:
        model = ArticleFile
        fields = ('file_field',)


class ArticleFileUploadForm(ModelForm):
    class Meta:
        model = ArticleFile
        fields = ('file',)


class ReviewCreateForm(ModelForm):
    q1 = forms.ChoiceField(choices=Q1_CHOICE,
                           label=QUESTIONS[0])
    q2 = forms.CharField(widget=forms.Textarea,
                         label=QUESTIONS[1])
    q3 = forms.CharField(widget=forms.Textarea,
                         label=QUESTIONS[2])
    q4 = forms.CharField(widget=forms.Textarea,
                         label=QUESTIONS[3])
    q5 = forms.ChoiceField(choices=Q5_CHOICE, label=QUESTIONS[4])

    q6 = forms.CharField(widget=forms.Textarea, label=QUESTIONS[5])

    class Meta:
        model = Review
        fields = ('q1', 'q2', 'q3', 'q4', 'q5', 'q6')


class TAuthorForm(forms.ModelForm):
    class Meta:
        model = TAuthor
        fields = ('email', 'first_name', 'last_name', 'university', 'department')
