from django.core.exceptions import PermissionDenied
from django.http import HttpResponse


def only_reviewer(func):
    def decorator(request, *args, **kwargs):
        if not request.user.user_role == 'REVIEWER':
            raise PermissionDenied
        return func(request, *args, **kwargs)

    return decorator


def only_author(func):
    def decorator(request, *args, **kwargs):
        if not request.user.user_role == 'AUTHOR':
            raise PermissionDenied
        return func(request, *args, **kwargs)

    return decorator


def only_editor(func):
    def decorator(request, *args, **kwargs):
        if not request.user.user_role == 'EDITOR':
            raise PermissionDenied
        return func(request, *args, **kwargs)

    return decorator


def allowed_users(allowed_roles=[]):
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):

            group = None
            if hasattr(request.user, 'user_role'):
                group = request.user.user_role

            if group in allowed_roles:
                return view_func(request, *args, **kwargs)

            else:
                return HttpResponse("You are not authorized")

        return wrapper_func

    return decorator
